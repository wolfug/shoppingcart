
<?php
    session_start();
   include "header.php";

   if (empty($_SESSION["shopping_cart"])) {
       header("Location: cart.php");
   }
    $errors = [];
   if (isset($_POST['checkout'])) {
       $name = $_POST["fullname"];
       if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
           $errors['fullname'] = "Only letters and white space allowed";
       }
       $email = $_POST["email"];
       if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
           $errors['email'] = "Invalid email format";
       }

       $address = $_POST["address"];
       if (!preg_match("/^[a-zA-Z ]*$/",$address)) {
           $errors['address'] = "Only letters and white space allowed";
       }
       $phone = $_POST["phone"];
       if (!validate_phone($phone)) {
           $errors['phone'] = "Phone incorrect";
       }

       $cart = $_POST["credit_cart"];
       if (!luhn_check($cart)) {
           $errors['credit_cart'] = "Credit cart incorrect";
       }

       if (!count($errors) > 0) {
           $_SESSION["info"] = $_POST;
           header("Location: print.php");

       }
   }

function validate_phone($number) {
    $number = preg_replace('/[^\d]/', '', $number);
    return preg_match('/^(0(2|3|4|7|8))?\d{8}$/', $number)
        || preg_match('/^1(3|8)00\d{6}$/', $number)
        || preg_match('/^13\d{4}$/', $number);
}

function luhn_check($number) {

    // Strip any non-digits (useful for credit card numbers with spaces and hyphens)
    $number=preg_replace('/\D/', '', $number);

    // Set the string length and parity
    $number_length=strlen($number);
    $parity=$number_length % 2;

    // Loop through each digit and do the maths
    $total=0;
    for ($i=0; $i<$number_length; $i++) {
        $digit=$number[$i];
        // Multiply alternate digits by two
        if ($i % 2 == $parity) {
            $digit*=2;
            // If the sum is two digits, add them together (in effect)
            if ($digit > 9) {
                $digit-=9;
            }
        }
        // Total up the digits
        $total+=$digit;
    }

    // If the total mod 10 equals 0, the number is valid
    return ($total % 10 == 0) ? TRUE : FALSE;

}


?>

<main>
    <?php
        $strError = '';
        if (count($errors) > 0) {
            foreach ($errors as $key => $cart) {
                $strError .= "<p style='text-align: center; color: red;'>{$key} : {$cart}</p>";
            }
        }
        echo $strError;
    ?>
    <form action="checkout.php" method="post">
			<p>
                <label for="fname" style="display: inline-block; width: 100px;"><i class="fa fa-user"></i> Full Name</label>
            <input type="text" id="fname" name="fullname" placeholder="your full name" value="<?php echo !empty($_POST['fullname']) ? $_POST['fullname'] : '';?>" required></p>
			<p><label for="email" style="display: inline-block; width: 100px;"><i class="fa fa-envelope"></i> Email</label>
            <input type="email" id="email" name="email" placeholder="email@example.com" value="<?php echo !empty($_POST['email']) ? $_POST['email'] : '';?>" required style="width: 50%;padding: 12px 20px;margin: 8px 0;display: inline-block;border: 1px solid #ccc;box-sizing: border-box;"></p>
			 <p><label for="adr" style="display: inline-block; width: 100px;"><i class="fa fa-address-card-o"></i> Address</label>
            <input type="text" id="adr" name="address" placeholder="your address" size="10" value="<?php echo !empty($_POST['address']) ? $_POST['address'] : '';?>" required></p>
			<p><label for="mobile" style="display: inline-block; width: 100px;"><i class="fa fa-mobile"></i> mobile: </label>
			<input type="tel" name="phone" maxlength="10" value="04" required style="width: 50%;padding: 12px 20px;margin: 8px 0;display: inline-block;border: 1px solid #ccc;box-sizing: border-box;"></p>
			<p><label for="ccnum" style="display: inline-block; width: 100px;">Credit card number</label>
            <input type="text" id="myInput" oninput="myFunction()" name="credit_cart" placeholder="---- ---- ---- ----" value="<?php echo !empty($_POST['credit_cart']) ? $_POST['credit_cart'] : '';?>" required />
            <p id="demo"></p>
			<p><label for="expiry" style="display: inline-block; width: 100px;"><i class="fa fa-expiry"></i> Expiry day:</label>
        <input type="date" id="start" name="expiry_day"
               value="<?php echo date("Y-m-d");?>"
               min="2018-11-11" max="2020-12-31" style="width: 50%;padding: 12px 20px;margin: 8px 0;display: inline-block;border: 1px solid #ccc;box-sizing: border-box;" /></p>
    <input type="submit" class="button-submit" name="checkout" style="margin-left: 100px" />
    </form>
    </div>




        <div>
                <div id="Checkout">
                        <a href="Checkout.html">
                                <img src="img/Checkout.jpg" width="150" height="150">
        </div>

 </main>

<script>
  function myFunction() {
    var letter = document.getElementById("myInput").value;
    var text;
    var n = letter.length;
    path="img\\visa.png"
    path1="img\\master.png"
    if (letter.startsWith('4') && n > 13 && n < 16) {
      document.getElementById("demo").innerHTML = '<img src="'+path+'"/>';

    }else if
    (letter.startsWith('2') && n > 13 && n < 16) {
      document.getElementById("demo").innerHTML = '<img src="'+path1+'"/>';
    } else {
      text = "-blank-";
      document.getElementById("demo").innerHTML = text;
    }


  }

</script>
<?include "footer.php";?>
