<?php
session_start();
include "header.php";

$str = file_get_contents('product.json');

$json = json_decode($str, true);

$products = $json['products'];

$result = [];

if (!empty($_SESSION["shopping_cart"])) {
    foreach ($_SESSION["shopping_cart"] as $key => $value) {
        foreach ($products as $k => $v) {
            if ($value['id'] == $v['id']) {
                $result[] = array_merge($v, $value);
            }
        }
    }
}

//if(isset($_POST["add_to_cart"]))
//    {
//    if(isset($_SESSION["shopping_cart"]))
//	{
//		$item_array_id = array_column($_SESSION["shopping_cart"], "item_id");
//		if(!in_array($_GET["id"], $item_array_id))
//		{
//			$count = count($_SESSION["shopping_cart"]);
//			$item_array = array(
//				'item_id'			=>	$_GET["id"],
//
//
//				'size'         => $_POST["item_size"],
//				'colors'		=>	$_POST["item_colors"],
//				'item_name'			=>	$_POST["hidden_name"],
//				'item_price'		=>	$_POST["hidden_price"],
//
//				'item_quantity'		=>	$_POST["qty"]
//			);
//			$_SESSION["shopping_cart"][$count] = $item_array;
//
//		}
//
//	}
//	else
//	{
//		$item_array = array(
//			'item_id'			=>	$_GET["id"],
//
//			'item_name'			=>	$_POST["hidden_name"],
//			'size'         => $_POST["item_size"],
//			'colors'		=>	$_POST["item_colors"],
//			'item_price'		=>	$_POST["hidden_price"],
//
//			'item_quantity'		=>	$_POST["qty"]
//		);
//		$_SESSION["shopping_cart"][0] = $item_array;
//	}
//}
//if (isset($_POST['check-out'])) { //when confirm the order
//   $count = count($_SESSION["shopping_cart"]);
//
//     for ($x=0; $x<$count; $x++) {
//     $_SESSION["shopping_cart"][$x]['item_quantity'] = $_POST['num'][$x];
//    }
//
//  header("Location: checkout.php");}
if(isset($_GET["action"]))
{
	if($_GET["action"] == "delete")
	{
		foreach($_SESSION["shopping_cart"] as $keys => $values)
		{
			if($values["id"] == $_GET["id"])
			{
				unset($_SESSION["shopping_cart"][$keys]);
                header("Location: cart.php");

			}
		}
	}
}
if(isset($_POST['update'])){
			$count = count($_SESSION["shopping_cart"]);

        for ($x=0; $x<$count; $x++) {
        $_SESSION["shopping_cart"][$x]['quantity'] = $_POST['num'][$x];
            header("Location: cart.php");

}
 }



if (isset($_POST['cancel'])) {
  unset($_SESSION['shopping_cart']);
  header("Location: products.php");
}

?>


    <main>
    	<h3>Order Details</h3>
			<form action="cart.php" method="post"><div class="table-cart">
				<table class="table-cart">
					<tr>
						<th width="30%">Item Name</th>
                        <th width="10%">Size  </th>
						<th width="10%">Quantity</th>
						<th class='price' width="20%">Price</th>

						<th class='sum' width="15%">Total</th>
						<th width="5%">Action</th>
					</tr>
					<?php
					if(count($result) > 0)
					{
						$total = 0;
						foreach($result as $keys => $values)
						{
					?>
					<tr>
						<td><?php echo $values["name"]; ?></td>
                        <td><?php echo $values["size"]; ?></td>
						<td><input type="number" id="qty" name="num[]" value=<?php echo $values["quantity"]?>></td>
						<td class='price'>$ <?php echo $values["price"]; ?></td>
						<td class='sum' >$ <?php echo number_format($values["quantity"] * $values["price"], 2 );?></td>
						<td><a href="cart.php?action=delete&id=<?php echo $values["id"]; ?>"><span class="text-danger">Remove</span></a></td>
					</tr>
					<?php
							$total = $total + ($values["quantity"] * $values["price"]);
						}
					?>
					<tr>
						<td id='total' colspan="3" align="right">Total</td>
						<td id='total' align="right">$ <?php echo number_format($total, 2); ?></td>
						<td></td>
						<td></td>
					</tr>
					<?php
					}
					?>

				</table>

			</div>
        <div>
            <a href="products.php" class="button-submit">Continue Shopping</a>
            <a href="checkout.php" class="button-submit">Checkout</a>
		 <input type="submit" class="button-submit" name="update" value="update" >
            <input type="submit" class="button-submit" name="cancel" value="cancel" >

        </form>
    </main>
<script>

function getTotal(){
    var total = 0;
    $('.sum').each(function(){
        total += parseFloat(this.innerHTML)
    });
    $('#total').text(total);
}

getTotal();

$('.qty').keyup(function(){
    var parent = $(this).parents('tr');
    var price = $('.price', parent);
    var sum = $('.sum', parent);
    var value = parseInt(this.value) * parseFloat(price.get(0).innerHTML||0);
    sum.text(value);
    getTotal();
})</script>
<?php
include "footer.php";?>



