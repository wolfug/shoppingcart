<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Fun Shop | Contact</title>
    <link rel="stylesheet" href="./css/style.css">

</head>

<body>

    <header>
        <div class="container">
            <div id="branding">
                <h1>
                    <span class="highlight">Fun Shop</span>
                </h1>
            </div class="current">
            <div id="shoppingcart">
                <a href="cart.php">
                    <img src="img/Shopping.png" width="40" height="30">
            </div>
            <nav>
              <ul>
                     <li class="current">
                        <a href="index.php">Home</a>
                    </li>
                    <li>
                        <a href="products.php">Products</a>
                    </li>
                    <li>
                        <a href="">SALES!!!</a>
                    </li>
                    <li>
                        <a href="">My Account</a>
                    </li>
                    <li>
                        <a href="">Contact</a>
                    </li>
                    <li>
                        <a href="">About</a>
                    </li>





                </ul>
            </nav>
        </div>


    </header>
 <div class="container">
    <label for="uname"><b>Username</b></label>
    <input type="text" placeholder="Enter Username/Email Address" name="uname" required>

    <label for="psw"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="psw" required>

    <button type="submit">Login</button>
    <label>
      <input type="checkbox" checked="checked" name="remember"> Remember me
    </label>
  </div>
	<Footer>
        <p> Chuong Pham Lam, s3660068 </p>
      <div>Disclaimer: This website is not a real website and is being developed as part of a School of Science Web Programming course at RMIT University in Melbourne, Australia.</div>

    </Footer>
</body>

</html>
