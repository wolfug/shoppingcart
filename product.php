<?php
session_start();
include "header.php";

if (empty($_GET['id'])) {
    header("Location: error.php");
}

$str = file_get_contents('product.json');

$json = json_decode($str, true);

$products = $json['products'];

$product = [];
$productDiff = [];
foreach ($products as $pro) {

    if ($pro['id'] == $_GET['id']) {
        $product = $pro;
    } else {
        $productDiff[] = $pro;
    }

}
if (!count($product) > 0) {
    header("Location: error.php");
}
if(isset($_POST["add_to_cart"]))
    {
    if(isset($_SESSION["shopping_cart"]))
	{
	    $checkProduct = false;
	    foreach ($_SESSION["shopping_cart"] as $key => $val) {
	        if ($_POST["id"] == $val['id']) {
                $checkProduct = true;
                $_SESSION["shopping_cart"][$key]['quantity'] += $_POST["quantity"];
            }
        }
        if ($checkProduct == false) {
            $_SESSION["shopping_cart"][] = [
                'id' => $_GET['id'],
                'size' => $_POST['size'],
                'quantity' => $_POST['quantity'],
            ];
            echo "<script>alert('Add cart success')</script>";
        }
	} else {
        $_SESSION["shopping_cart"][] = [
            'id' => $_GET['id'],
            'size' => $_POST['size'],
            'quantity' => $_POST['quantity'],
        ];
        echo "<script>alert('Add cart success')</script>";
    }
}

?>


<main style="text-align: center;">

    <form method="post" action="product.php?id=<?php echo $_GET['id']; ?>">
        <div id="Product_1" style="text-align: center;">
            <input type="hidden" min="1" max="999" name="id" value="<?php echo $_GET['id']; ?>" />
            <div class="Box1"><img src="<?php echo $product['image']; ?>" alt="1" height="400" width="600"></div>
            <div class="Box2">
                <h1>Converse X Hello Kitty One Star Junior </h1>
                <span>Select size: </span>
                <select name="size" required>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                </select>
                <span>Enter quantity: </span>
                <input type="number" min="1" max="999" value="1" required name="quantity" style="display: inline;width:100px" />
            </div>

        </div>
        <p><?php echo $product['desc']; ?></p>
        <input type="submit" class="add-to-cart" name='add_to_cart' value="ADD TO CART"></form>
    <h4>You may like:</h4>
    <div>
        <?php foreach ($productDiff as $key => $pro) {
            if ($key < 4) {
                ?>
                <span class="Box2_1">
                    <a href="product.php?id=<?php echo $pro['id']; ?>">
                        <img src="<?php echo $pro['image']; ?>" alt="1" height="100" width="100" />
                    </a>
                </span>
            <?php }
        } ?>
    </div>


    </div>
    <div class="Products_2">
        <h3>Hello Kitty Forever</h3>
        <p>
        </p>

        <?php
        include "footer.php"; ?>
