<?php
   if(isset($_POST["add_to_cart"]))
    {
    if(isset($_SESSION["shopping_cart"]))
	{
		$item_array_id = array_column($_SESSION["shopping_cart"], "item_id");
		if(!in_array($_POST["id"], $item_array_id))
		{
			$count = count($_SESSION["shopping_cart"]);
			$item_array = array(
				'item_id'			=>	$_POST["hidden_id"],
				'item_name'			=>	$_POST["hidden_name"],
                 'size'         => $_POST["item_size"],
				'item_price'		=>	$_POST["hidden_price"],
				'item_quantity'		=>	$_POST["qty"]
			);
			$_SESSION["shopping_cart"][$count] = $item_array;
           
		}
		else
		{
			echo '<script>alert("Item Already Added")';
		}
	}
	else
	{
		$item_array = array(
			'item_id'			=>	$_POST["hidden_id"],
			'item_name'			=>	$_POST["hidden_name"],
             'size'         => $_POST["item_size"],
			'item_price'		=>	$_POST["hidden_price"],
           
			'item_quantity'		=>	$_POST["qty"]
		);
		$_SESSION["shopping_cart"][0] = $item_array;
    
	}
}
if(isset($_GET["action"]))
{
	if($_GET["action"] == "delete")
	{
		foreach($_SESSION["shopping_cart"] as $keys => $values)
		{
			if($values["item_id"] == $_GET["id"])
			{
				unset($_SESSION["shopping_cart"][$keys]);
				echo '<script>alert("Item Removed")</script>';
				
			}
		}
	}
}
?>


    <main>

  
        <div class="title"> <h2>Products list</h2></div>
       
        <div class="item">
            <div class="column"><a href=""><img class="products" src="media/pic4.jpg" ></a>
                <form method="post" action="cart.php?action=add&id=1">
                    <h2><a href="">CORITIV</a></h2>
                    <p>Style: Triangle Bikini</p>
                    <p>$199.99</p><a>Select size: </a>
                    <select name = "item_size"  required>
                                <option value="Small">S</option>
                                <option value="Medium">M</option>
                                <option value="Large">L</option>
    
                            </select>
                    <br><br>
                    <a> Quantity:</a>
                    <div class="quantity-box">
                        <input type="number" name="qty" min="1" required></div><br> <input type="submit" name="add_to_cart" style="margin-top:5px;" class="button-submit" value="Add to Cart"><br>
                    <input type="hidden" name="hidden_name" value="CORITIV">
                    <input type="hidden" name="hidden_id" value="s111">
                    <input type="hidden" name="hidden_price" value="199.99">
                </form>
            </div>


            <div class="column"><a href=""><img class="products" src="media/pic5.jpg" ></a>
                <form method="post" action="cart.php?action=add&id=2">
                    <h2><a href="">RUNDO</a></h2>
                    <p>Style: Conjoined boxer swimsuit</p>
                    <p>$29.99</p> <a>Select size: </a><select name = "item_size" required>
                                <option value="Small">S</option>
                                <option value="Medium">M</option>
                                <option value="Large">L</option>
    
                            </select>
                    <br><br>
                    <a> Quantity:</a>
                    <div class="quantity-box">
                        <input type="number" name="qty" min="1" required></div><br><input type="submit" name="add_to_cart" style="margin-top:5px;" class="button-submit" value="Add to Cart"><br>
                    <input type="hidden" name="hidden_name" value="RUNDO">
                    <input type="hidden" name="hidden_id" value="s222">
                    <input type="hidden" name="hidden_price" value="29.99">
                </form>
            </div>
            <div class="column"><a href=""><img class="products" src="media/pic9.jpg"></a>
                <form method="post" action="cart.php?action=add&id=3">
                    <h2><a href="">YINGFA</a></h2>
                    <p>Style: Triangle one-piece swimsuit</p>
                    <p>$14.99</p><a>Select size: </a> <select name = "item_size" required>
                                <option value="Small">S</option>
                                <option value="Medium">M</option>
                                <option value="Large">L</option>
    
                            </select>
                    <br><br>
                    <a> Quantity:</a>
                    <div class="quantity-box">
                        <input type="number" name="qty" min="1" required></div><br><input type="submit" name="add_to_cart" style="margin-top:5px;" class="button-submit" value="Add to Cart"><br>
                    <input type="hidden" name="hidden_name" value="YINGFA">
                    <input type="hidden" name="hidden_id" value="s333">
                    <input type="hidden" name="hidden_price" value="14.99">
                </form>
            </div>

        </div>

    </main>

    <!--
 $rows; $cells; 
$fp = fopen('products.txt','r'); 
while ( $cells = fgetcsv($fp, 0, "\t") ) { 
     $rows[] = $cells; 
 } fclose($fp); 

-->

    <?php   
    require_once("tools.php");
    end_module();
?>
